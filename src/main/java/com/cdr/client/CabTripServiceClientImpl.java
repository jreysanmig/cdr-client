package com.cdr.client;

import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import com.cdr.client.model.CabTripCount;

public class CabTripServiceClientImpl implements CabTripServiceClient {
	
	String BASE_URI = "http://localhost:8080/trips";

	@Override
	public List<CabTripCount> getMedallionsSummary(String[] medallions, String pickupDate) {
		return getMedallionsSummary(medallions,pickupDate,false);
	}

	@Override
	public List<CabTripCount> getMedallionsSummary(String[] medallions, String pickupDate, boolean ignoreCache) {
		String medallionsStr = "cab/" + String.join(",",medallions);
		String pickupStr = "pickup/" + pickupDate;
		
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target(BASE_URI).path(medallionsStr).path(pickupStr);
		if(ignoreCache) {
			webTarget.path(""+ignoreCache);
		}
		
		List<CabTripCount> response = webTarget.request(MediaType.APPLICATION_JSON).get(new GenericType<List<CabTripCount>>() {});
		
		return response;
	}

	@Override
	public void deleteCache() {
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target(BASE_URI).path("cache-clear");
		String response = webTarget.request(MediaType.APPLICATION_JSON).get(new GenericType<String>() {});
		System.out.println(response);
	}

}
