package com.cdr.client;

import java.util.List;
import java.util.Scanner;

import com.cdr.client.model.CabTripCount;

public class ClientApp {

	public static final String COMMAND_INIT = "init";
	public static final String COMMAND_COUNT = "count";
	public static final String COMMAND_EXIT = "exit";
	public static final String COMMAND_CLEAR = "clear";

	public static void main(String[] args) {

		String command = COMMAND_INIT;
		Scanner input = new Scanner(System.in);

		CabTripServiceClient tripService = new CabTripServiceClientImpl();

		System.out.println("******************************");
		System.out.println("******************************");
		System.out.println("** CAB TRIPS SERVICE CLIENT **");
		System.out.println("******************************");
		System.out.println("******************************");
		System.out.println("count - get the count of trips per cab on a particular date");
		System.out.println("clear - clear the cache");
		System.out.println("exit - exit from the client app");
		System.out.println();
		System.out.println();

		while (!COMMAND_EXIT.equalsIgnoreCase(command)) {
			System.out.print("\n> ");
			command = input.nextLine();

			switch (command) {
			case COMMAND_EXIT:
				System.out.println("Closing the client...");
				break;
			case COMMAND_CLEAR:
				tripService.deleteCache();
				break;
			case COMMAND_COUNT:
				System.out.print(">>Enter medallion/s separated by comma (,): ");
				String medallions = input.nextLine();
				String[] medals = medallions.split(",");
				System.out.print(">>Enter pick up date (YYYY-MM-DD): ");
				String pickupDate = input.nextLine();
				System.out.print(">>Fresh data? (y/n): ");
				String freshData = input.nextLine();

				List<CabTripCount> tripsSummary;
				if ("y".equalsIgnoreCase(freshData)) {
					tripsSummary = tripService.getMedallionsSummary(medals, pickupDate, true);
				} else {
					tripsSummary = tripService.getMedallionsSummary(medals, pickupDate);
				}
				printSummary(tripsSummary);
				break;
			}
		}

		input.close();

	}

	public static void printSummary(List<CabTripCount> summary) {
		System.out.println();
		System.out.println("*** SUMMARY OF TRIPS (PER CAB) *** ");
		System.out.println("[Medallion]" + "\t\t\t\t" + "[Trips]");
		summary.forEach(ctc -> System.out.println(ctc.getMedallion() + "\t" + ctc.getCount()));
		System.out.println();
	}

}
