package com.cdr.client.model;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class CabTripCount {
	
	private Date pickupDateTime;
	private String medallion;
	private long count;
	
	public CabTripCount() {}
	
	public CabTripCount(Date pickupDateTime, String medallion, long count) {
		this.pickupDateTime = pickupDateTime;
		this.medallion = medallion;
		this.count = count;
	}
	
	public Date getPickupDateTime() {
		return pickupDateTime;
	}

	public void setPickupDateTime(Date pickupDateTime) {
		this.pickupDateTime = pickupDateTime;
	}

	public String getMedallion() {
		return medallion;
	}
	public void setMedallion(String medallion) {
		this.medallion = medallion;
	}
	public long getCount() {
		return count;
	}
	public void setCount(long count) {
		this.count = count;
	}

}
