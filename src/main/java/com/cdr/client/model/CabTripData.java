package com.cdr.client.model;

import java.util.Date;

public class CabTripData {
	
	private String medallion;
	
	private String hackLicense;	

	private String vendorId;
	
	private int rateCode;
	
	private String storeAndFwdFlag;
	
	private Date pickupDateTime;
	
	private Date dropoffDateTime;
	
	private int passengerCount;
	
	private int tripTimeInSecs;
	
	private double tripDistance;

	public String getMedallion() {
		return medallion;
	}

	public void setMedallion(String medallion) {
		this.medallion = medallion;
	}

	public String getHackLicense() {
		return hackLicense;
	}

	public void setHackLicense(String hackLicense) {
		this.hackLicense = hackLicense;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public int getRateCode() {
		return rateCode;
	}

	public void setRateCode(int rateCode) {
		this.rateCode = rateCode;
	}

	public String getStoreAndFwdFlag() {
		return storeAndFwdFlag;
	}

	public void setStoreAndFwdFlag(String storeAndFwdFlag) {
		this.storeAndFwdFlag = storeAndFwdFlag;
	}

	public Date getPickupDateTime() {
		return pickupDateTime;
	}

	public void setPickupDateTime(Date pickupDateTime) {
		this.pickupDateTime = pickupDateTime;
	}

	public Date getDropoffDateTime() {
		return dropoffDateTime;
	}

	public void setDropoffDateTime(Date dropoffDateTime) {
		this.dropoffDateTime = dropoffDateTime;
	}

	public int getPassengerCount() {
		return passengerCount;
	}

	public void setPassengerCount(int passengerCount) {
		this.passengerCount = passengerCount;
	}

	public int getTripTimeInSecs() {
		return tripTimeInSecs;
	}

	public void setTripTimeInSecs(int tripTimeInSecs) {
		this.tripTimeInSecs = tripTimeInSecs;
	}

	public double getTripDistance() {
		return tripDistance;
	}

	public void setTripDistance(double tripDistance) {
		this.tripDistance = tripDistance;
	}
	
}
