package com.cdr.client;

import java.util.List;

import com.cdr.client.model.CabTripCount;

public interface CabTripServiceClient {

	void deleteCache();

	List<CabTripCount> getMedallionsSummary(String[] medallions, String pickupDate);

	List<CabTripCount> getMedallionsSummary(String[] medallions, String pickupDate, boolean ignoreCache);

}
